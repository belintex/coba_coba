<!-- ==library==
1. bootstrap
2. jquery
3. phpmail (swiftmailer)
4. sweetalert2
5. dompdf
6. gmail
==//library== -->
<!DOCTYPE html>
<html>
<head>
	<title>Dasar Jquery</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="vendor/sweetalert2/dist/sweetalert2.min.css">
	<style type="text/css">
		/*.container*/
		/*#ini_kirim*/
	</style>
</head>
<body>
	<div class="container">
		<h1>Form Pengiriman Harga Produk</h1>
		<form id="form">
			<div class="form-group">
				<label>Nama</label>
				<input type="text" name="nama" id="nama" class="form-control" name="">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="email" name="email" id="email" class="form-control" name="">
			</div>
			<div class="form-group">
			<label>Pilih Maunya</label>
				<select class="form-control" name="mau" id="mau">
					<option value="laptop">Laptop ASUS - Rp 1.000.000 </option>
					<option value="hp">HP OPPO - Rp 200.000</option>
				</select>
			</div>
			<div class="form-group">
				<label>Jumlah</label>
				<input type="number" name="jml" id="jml" class="form-control">
			</div>
			<button type="button" class="btn btn-success" id="ini_kirim">Kirim</button>
		</form>
	</div>
<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="vendor/sweetalert2/dist/sweetalert2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#ini_kirim').click(function(){
		// alert()
		// console.log("tes")
		$.ajax({
	        url: 'proses.php', 
	        type: 'post',
	        // data: $('#form').serialize()
	        data: {nama: $('#nama').val(), email: $('#email').val(), mau:$('#mau').val(), jml:$('#jml').val()}
	    })
	    .done(function(data){
	        // show the response  
	       	Swal.fire(
			  data,
			  '',
			  'success'
			) 
	    })
    })
})
</script>
</body>
</html>