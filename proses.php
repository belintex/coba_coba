<?php
include 'vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Dompdf\Dompdf;

class Pdf extends Dompdf{
	public function __construct(){
		parent::__construct();
	}
}
// $satu = "ini didalam variabel";

// echo "ini sudah didalam proses dot php";
// echo $satu;
$nama  = $_POST['nama'];
$email = $_POST['email'];
$mau   = $_POST['mau'];
$jml   = $_POST['jml'];

if ($mau=="laptop") {
	$namabrg = "Laptop ASUS";
	$total = 1000000*$jml;
}
if ($mau=="hp") {
	$namabrg = "HP OPPO";
	$total = 200000*$jml;
}

$message   = "";
$html_code = "
				<h1>Harga barang : ".$namabrg."</h1>
				<h2>Dengan jumlah : ".$jml."</h2>
				<h3>Seharga : Rp ".number_format($total, 0, ".", ".")."</h3>
			";

$pdf = new Pdf();
$file_name = 'pdf/'.$nama.'.pdf';
$pdf->load_html($html_code);
$pdf->render();
$file = $pdf->output();
file_put_contents($file_name, $file);

// parameter 1 host email
// parameter 2 port host
// parameter 3 tls/ssl
$transport = (new Swift_SmtpTransport('host', port, 'tls'))
	    ->setUsername('email')
	    ->setPassword('password');
$mailer = new Swift_Mailer($transport);
// pesan
$message = (new Swift_Message('Judul produk buat kamu'))
	    ->setFrom(['email dari' => 'nama pengirim'])
	    ->setTo(['email penerima'])
	    ->setBody('Detail Produk kamu didalam PDF ini')
	    ->attach(Swift_Attachment::fromPath($file_name))
	    ;
if ($mailer->send($message)) {
	echo "Berhasil Pengiriman Email";
}else{
	echo "Gagal Pengiriman Email";
}


?>